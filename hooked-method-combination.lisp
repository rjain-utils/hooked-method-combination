(defpackage :hooked-method-combination
  (:use :cl)
  (:export #:hooked-method-combination
           #:set-hook-name
           #:define-hook-method))

(in-package :hooked-method-combination)


(define-method-combination hooked-method-combination ()
    ((around (:around))
     (around-hooks (:around *))
     (before (:before))
     (before-hooks (:before *))
     (primary () :required t)
     (after (:after))
     (after-hooks (:after *)))
  "Copied and tweaked from the example of the standard
method-combination in the CLHS.

This method-combination behaves just like the standard
method-combination except that :around, :before, and :after methods
accept a second qualifier, which should be a private symbol that
uniquely indicates the source of this method. When a method is defined
with the same qualifier and specializer pattern as an existing method on
the same generic-function, the old method is discarded. This behavior of
CLOS can be exploited to allow third party extensions to 'hook' onto or
'advise' pivotal functions of a package without losing the power of CLOS
and still allow the extension to be reloaded or patched without adding
the additional functionality twice.

'Hook methods', those with two qualifiers, precede all of the non-hook
methods and precede each other according to standard specializer
precedence. However, the ordering of the hook methods with the same
specializers is not specified.

This method-combination may be somewhat reminiscient of aspect-oriented
programming, but it does not implement many of the features of AOP and
definitely not those of fully dynamic AOP."
  (flet ((call-methods (methods)
           (mapcar #'(lambda (method)
                       `(call-method ,method))
                   methods)))
    (let ((form (if (or before after (rest primary))
                    `(multiple-value-prog1
                      (progn ,@(call-methods before-hooks)
                             ,@(call-methods before)
                             (call-method ,(first primary)
                                          ,(rest primary)))
                      ,@(call-methods (reverse after))
                      ,@(call-methods (reverse after-hooks)))
                    `(call-method ,(first primary)))))
      (if around
          `(call-method ,(first around-hooks)
            (,@(rest around-hooks)
             ,@around
             (make-method ,form)))
          form))))

(defvar *hook-names* (make-hash-table :test 'eq)
  "package -> current hook name (symbol)")

(defmacro set-hook-name (symbol)
  (if (or (keywordp symbol)
          (multiple-value-bind (found-symbol status)
              (find-symbol (symbol-name symbol) (symbol-package symbol))
            (not (and (eq found-symbol symbol)
                      (eq status :internal)))))
      (error "Keywords and public symbols are too prone to ~
clashing. ~_Please use an internal symbol in your own package to ~
identify your hooks.")
      `(eval-when (:evaluate :compile-toplevel)
        (setf (gethash *package* *hook-names*) symbol))))

(defmacro define-hook-method (function qualifier lambda-list &body body)
  `(defmethod ,function
    ,qualifier ,(gethash *package* *hook-names*)
    ,lambda-list
    ,@body))
